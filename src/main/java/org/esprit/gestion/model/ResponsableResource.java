package org.esprit.gestion.model;
import java.io.Serializable;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity

public class ResponsableResource implements Serializable{

    private  int idResponsableResource;
    private String nom;
    private String prenom;
    private String cin ;
    private int telephone;
    private String email;



    public ResponsableResource(){

    }

    public ResponsableResource(int idResponsableResource, String nom, String prenom, String cin, int telephone, String email) {
        this.idResponsableResource = idResponsableResource;
        this.nom = nom;
        this.prenom = prenom;
        this.cin = cin;
        this.telephone = telephone;
        this.email = email;
    }

    public int getIdResponsableResource() {
        return idResponsableResource;
    }

    public void setIdResponsableResource(int idResponsableResource) {
        this.idResponsableResource = idResponsableResource;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public int getTelephone() {
        return telephone;
    }

    public void setTelephone(int telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}