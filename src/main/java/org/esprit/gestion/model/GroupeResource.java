package org.esprit.gestion.model;

import javax.persistence.Entity;

@Entity
public class GroupeResource {

    private int idGroupeRessource;
    private String nom;
private int f;

    public GroupeResource(int idGroupeRessource, String nom) {
        this.idGroupeRessource = idGroupeRessource;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "GroupeRessourece{" +
                "idGroupeRessource=" + idGroupeRessource +
                ", nom='" + nom + '\'' +
                '}';
    }

    public void setIdGroupeRessource(int idGroupeRessource) {
        this.idGroupeRessource = idGroupeRessource;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getIdGroupeRessource() {

        return idGroupeRessource;
    }

    public String getNom() {
        return nom;
    }
}