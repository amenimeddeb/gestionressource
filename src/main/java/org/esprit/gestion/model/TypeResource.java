package org.esprit.gestion.model;

import java.io.Serializable;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
@Entity

public class TypeResource implements Serializable {
    private int idTypeResource;
    private String nom;
    private List<Resource> resources;
    private GroupeResource groupeResource;
         //Fsdfsdfsdfsdf
    public TypeResource() {
        super();
    }

    public int getIdTypeResource() {
        return idTypeResource;
    }

    public void setIdTypeResource(int idTypeResource) {
        this.idTypeResource = idTypeResource;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @OneToMany(mappedBy="typeResource")
    public List<Resource> getResources() {
        return resources;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public GroupeResource getGroupeResource() {
        return groupeResource;
    }

    public void setGroupeResource(GroupeResource groupeResource) {
        this.groupeResource = groupeResource;
    }
}
