package org.esprit.gestion.model;

import org.esprit.formation.Seance;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Resource implements Serializable{
    private  int idRessource;
    private String label;
    private String designation;
    private String couleur ;
    private String numeroSerie;
    private String model;
    private String marque;
    private TypeResource typeresource;
    private Seance seance;

    public Seance getSeance() {
        return seance;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }

    public Resource() {
    }

    public Resource(int idRessource, String label, String designation, String couleur, String numeroSerie, String model, String marque, TypeResource typeresource, Seance seance) {
        this.idRessource = idRessource;
        this.label = label;
        this.designation = designation;
        this.couleur = couleur;
        this.numeroSerie = numeroSerie;
        this.model = model;
        this.marque = marque;
        this.typeresource = typeresource;
        this.seance = seance;
    }

    public int getIdRessource() {
        return idRessource;
    }

    public void setIdRessource(int idRessource) {
        this.idRessource = idRessource;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public String getNumeroSerie() {
        return numeroSerie;
    }

    public void setNumeroSerie(String numeroSerie) {
        this.numeroSerie = numeroSerie;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }
    @OneToMany()
    public TypeResource getTyperesource() {
        return typeresource;
    }

    public void setTyperesource(TypeResource typeresource) {
        this.typeresource = typeresource;
    }
}